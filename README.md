# Cheatsheet sobre o sensacional Git

## Sobre 

Conteúdo criado durante o treinamento Descomplicando o Git.
O conteudo é para auxiliar todos que estão na jornada de aprender e dominar o Git.

## Conteúdo

### Git Config

Para adicionar o seu nome, ou seja , o nome que ira aparecer com o autor de seus commits.

```bash
    git confi --global user.name "Nome do usuário"
```

Para adicionar o email do autor, faça:

```bash
    git config --global user.email "marciothadeu1984@gmail.com.br"
```

Para configurar qual será o seu editor de texto padrão, execute o seguinte comando:

```bash
    git confi --global --system editor vim
```

Para editar o arquivo de configuração do git:

```bash
    git config --global --edit
```

O arquivo de configuração GLOBAL do Git:

```bash
    vim $HOME/.gitconfig
```

O arquivo de configuração SYSTEM do Git:

```bash
    vim /etc/gitconfig
```

O arquivo de configuração LOCAL do Git:

```bash
    vim SEU_REPO/.git/config
```

Para listar as configurações do Git:

```bash
    git config --global --list
    git config --system --list
    git config --local --list
```
Onde:

```bash
    --global --list => Lista as configurações contida no arquivo $HOME/.gitconfig
    --system --list => Lista as configuraçõe contida no arquivo /etc/gitconfig
    --local --list => Lista as configurações contidas no arquivo SEU_REPO/.git/config
```

### Iniciando no Git

Para  remover o repositório remoto chamado "origin" do seu repositório Git local:

```bash
     git remote remove origin # é utilizado para remover o repositório remoto chamado "origin" do seu repositório Git local. Isso significa que o seu repositório local não estará mais vinculado ao repositório remoto "origin", e você não poderá mais enviar ou receber dados desse repositório remoto.
```

Para exibir as informações dos repositórios remotos configurados no seu repositório Git local:

```bash
    git remote -v #  lista o nome dos repositórios remotos e as URLs associadas a eles, permitindo que você veja para onde seu repositório está configurado para enviar e receber dados.
```

Para adiconar um arquivo no INDEX:

```bash
    git add <nome-do-arquivo> # substitua <nome-do-arquivo> pelo nome do arquivo que você deseja adicionar. Esse comando irá adicionar o arquivo ao INDEX, preparando-o para ser incluído no próximo commit. Você também pode usar o comando "git add". para adicionar todos os arquivos modificados e novos no diretório atual ao INDEX.
    git add . # Adiciona todos os arquivos desse diretório
```

Para clonar um repositório remoto:

```bash
    git clone ENDEREÇO_DO_REPO # lembre-se de copiar o endereço completo
```

Para adiconar um arquivo no HEAD:

```bash
    gitcommit -am "mensagem descritiva do commit" #adiciona todos os arquivos do INDEX para o HEAD
    git commit -m "mensagem descritiva do commit" NOME_DO_ARQUIVO # adiciona o arquivo ao HEAD
```

Para atualizar a branch local atual ou principal:

```bash
    git pull origin #  buscar as atualizações mais recentes de um repositório remoto e mesclá-las com o seu branch local atual
```

Para criar uma nova branch e alternar de branchs, ou adiconar as suas mudanças para o repositório do Git remoto:

```bash
    git push 
    git push origin <branch> # adicona ao servidor remoto na branch especificada
    git branch <branch> # Criar uma nova branch
    git checkout <nome-da-branch> # Alternar para a nova branch
    git checkout -b <nome-da-branch> # Alternar para a nova branch
    git branch -d <branch>  # Deletar branch local
    git branch -D <branch> # Forçar a exclusão da branch local
    git push origin --delete <branch> # # Deletar branch remota
    git merge <branch> # para mesclar as alterações de uma branch para outra. Ele combina as alterações feitas em uma branch com a branch atual em que você está

```

Para verificar o status do repositório local:

```bash
    git status
```

Para verificar os logs dos nossos commits:

```bash
    git log 
    git log -2 # para visualizar os dois ultimos commits
    git log --online # para exibir o histórico de commits de um repositório Git de forma resumida exibe cada commit em uma única linha, mostrando apenas o hash do commit e a mensagem descritiva.
    git log --raw # para exibir o histórico de commits de um repositório Git no formato "raw". Ele mostra informações detalhadas sobre as alterações de cada commit, incluindo as mudanças de arquivos, os modos de alteração (adicionado, modificado, removido) e os hashes dos commits.
    git log --pretty=full #  para exibir o histórico de commits de um repositório Git com um formato detalhado. Ele mostra informações completas sobre cada commit, incluindo o autor, a data, a mensagem do commit e as alterações realizadas.
    git log -- follow README.md # O parâmetro --follow permite que o Git siga as alterações do arquivo mesmo se ele for renomeado. Isso garante que o histórico de commits seja exibido corretamente.Ao executar o comando acima, você verá o histórico de commits relacionados ao arquivo README.md, mostrando o autor, a data, a mensagem do commit e outras informações relevantes. Isso pode ajudar a entender as alterações feitas no arquivo ao longo do tempo.
    git log --stat # para exibir  informações estatísticas resumidas sobre cada commit no histórico do Git. Ele mostra estatísticas sobre quais arquivos foram modificados em cada commit, bem como as linhas adicionadas e removidas em cada arquivo.
    git log --author=Thadeu # para exibir  apenas os commits feitos pelo autor especificado, no caso, "Thadeu". Lembre-se de substituir "Thadeu" pelo nome do autor que você deseja filtrar.
    git log --author=^John # Você também pode usar expressões regulares para fazer uma correspondência parcial no nome do autor. Por exemplo, se você quiser filtrar todos os autores cujo nome começa com "John", você pode usar o seguinte comando:
```

```bash
     git diff # Exibe as mudanças entre os commits, o commit, a árvore de trabalho, etc
```