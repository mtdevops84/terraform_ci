variable "ec2_region" {
  default = "us-east-1"
}

variable "ec2_keypair_name" {
  default = "gitlab-test"
}

variable "ec2_instance_type" {
  default = "t2.micro"
}

variable "ec2_image_id" {
  default = "ami-0261755bbcb8c4a84"
}

variable "ec2_tags" {
  default = "Descomplicando o Gitlab"
}

variable "ec2_instance_count" {
  default = "1"
}
